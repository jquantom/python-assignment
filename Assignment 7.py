Input_Number = int(input('Please Enter the Number: '))

Reminder = Input_Number % 3

if Reminder == 0 :
    print(f"The Given Number {Input_Number} is divisible by 3")
else:
    print(f"The Given Number {Input_Number} is not divisible by 3")