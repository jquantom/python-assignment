Input_Number = int(input('Please Enter the Number: '))

Mid_Number = Input_Number // 2
cnt = 0

for i in range(2, Mid_Number):
    if Input_Number % i == 0:
        cnt += 1

if Input_Number // Mid_Number == 0:
    cnt += 1

if cnt > 0:
    print(f"The Given Number {Input_Number} is not a Prime Number")
else:
    print(f"The Given Number {Input_Number} is a Prime Number")