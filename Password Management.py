class BasePasswordManager:

    def __init__(self):
        self.old_passwords = list()
        #  self.old_passwords = ['abc', '123', 'abc!@12']

    def get_password(self):
        if self.old_passwords:
            return self.old_passwords[-1]
        else:
            raise IndexError("No values")

    def is_correct(self, usr_pwd):
        try:
            usrp = self.get_password()
            return usrp == usr_pwd
        except IndexError as ie:
            raise IndexError("No values")


class PasswordManager(BasePasswordManager):
    __level_0 = 0
    __level_1 = 1
    __level_2 = 2
    __pwd_len = 6

    def __init__(self):
        super().__init__()

    def set_password(self, usr_pwd):
        if not isinstance(usr_pwd, str):
            raise Exception(f"Password should be a string")

        isc = None
        try:
            isc = self.is_correct(usr_pwd)
        except IndexError:
            isc = False

        if len(usr_pwd) >= self.__pwd_len and self.get_level(usr_pwd) > 1 and not isc:
            self.old_passwords.append(usr_pwd)
            print(f'password {self.old_passwords[-1]} is updated successfully')
        else:
            print("password does not meet the highest level criteria and cannot be updated")

    def get_level(self, password):
        if not isinstance(password, str):
            raise Exception("Password should be a string")
        if password.isalpha() or password.isnumeric():
            return self.__level_0
        if password.isalnum():
            return self.__level_1

        return self.__level_2


def main():
    pwdmgr = PasswordManager()
    while True:
        pwd = input('Enter the password: ')
        if pwd == 'exit':
            break
        pwdmgr.set_password(pwd)



if __name__ == "__main__":
    main()
